package gov.nasa.rover;

import gov.nasa.rover.design.Instruction;
import gov.nasa.rover.design.NavigationalSystemWarningException;
import gov.nasa.rover.design.Orientation;
import gov.nasa.rover.design.Vector;
import gov.nasa.rover.implementation.MarsRoverPrototype;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by hng on 11/20/17.
 */
public class TestRoverTest {

    public static final int X_MAX = 5;
    public static final int Y_MAX = 5;

    MarsRoverPrototype elsa;
    MarsRoverPrototype austin;

    @Before
    public void setUp() throws Exception {
        elsa = new MarsRoverPrototype("Elsa");
        austin = new MarsRoverPrototype("Austin");

        // load map data
        elsa.loadMap(X_MAX, Y_MAX);
        austin.loadMap(X_MAX, Y_MAX);
    }

    @Test
    public void TestRover() throws Exception, NavigationalSystemWarningException {

        //12N
        //LMLMLMLMM
        elsa.landing(new Vector(1,2, Orientation.N));
        elsa.command(Instruction.L);
        elsa.command(Instruction.M);
        elsa.command(Instruction.L);
        elsa.command(Instruction.M);
        elsa.command(Instruction.L);
        elsa.command(Instruction.M);
        elsa.command(Instruction.L);
        elsa.command(Instruction.M);
        elsa.command(Instruction.M);
        elsa.report();
        // 33E
        //MMRMMRMRRM
        austin.landing(new Vector(3,3, Orientation.E));
        austin.command(Instruction.M);
        austin.command(Instruction.M);
        austin.command(Instruction.R);
        austin.command(Instruction.M);
        austin.command(Instruction.M);
        austin.command(Instruction.R);
        austin.command(Instruction.M);
        austin.command(Instruction.R);
        austin.command(Instruction.R);
        austin.command(Instruction.M);
        austin.report();
    }
}