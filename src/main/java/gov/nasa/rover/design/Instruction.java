package gov.nasa.rover.design;

/**
 * Created by hng on 11/20/17.
 */
public enum Instruction {
    L(0), R(0), M(1);

    private int step;

    public int getStep() {
        return step;
    }

    Instruction(int step) {
        this.step = step;
    }
}
