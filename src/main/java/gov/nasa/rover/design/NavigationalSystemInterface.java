package gov.nasa.rover.design;

/**
 * Created by hng on 11/20/17.
 */
public interface NavigationalSystemInterface {

    /**
     * upload map data to navigational system
     * @param xMax
     * @param yMax
     */
    void loadMap(int xMax, int yMax);
    /**
     * decodes instruction and updates navigational systems current position and orientation
     * @param instruction
     */
    void command(Instruction instruction) throws NavigationalSystemWarningException;

    /**
     * reset navigational system to provided vector
     * @param vector
     */
    void landing(Vector vector);

    /**
     * report current known position
     */
    void report();
}
