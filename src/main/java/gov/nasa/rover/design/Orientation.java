package gov.nasa.rover.design;

/**
 * Created by hng on 11/20/17.
 */
public enum Orientation {
    N, S, E, W
}
