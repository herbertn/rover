package gov.nasa.rover.design;

/**
 * Created by hng on 11/20/17.
 */
public class NavigationalSystemWarningException extends Throwable {

    /**
     * builtin generic warning alert for all general purposes
     * @param msg
     */
    public NavigationalSystemWarningException(String msg) {
        System.out.println(msg);
    }
}
