package gov.nasa.rover.implementation;

import gov.nasa.rover.design.*;

/**
 * Created by hng on 11/20/17.
 */
public class MarsRoverPrototype implements NavigationalSystemInterface {

    private String name;
    private int currentX;
    private int currentY;
    private Orientation currentOrientation;

    private int mapMaxX;
    private int mapMaxY;

    public MarsRoverPrototype(String name) {
        this.name = name;
    }

    public void loadMap(int xMax, int yMax) {
        this.mapMaxX = xMax;
        this.mapMaxY = yMax;
    }

    public void command(Instruction instruction) throws NavigationalSystemWarningException {
        if (instruction == Instruction.M) {
            switch (currentOrientation) {
                case N:
                    currentY += 1;
                case S:
                    currentY -= 1;
                case W:
                    currentX -= 1;
                case E:
                    currentX += 1;
                default:
                    // do nothing
            }

            // disable our Navigational System Warning system for now
//            if (currentX < 0) {
//                currentX += 1;
//                throw new NavigationalSystemWarningException("Cannot go beyond the map");
//            }
//            if (currentY < 0) {
//                currentY += 1;
//                throw new NavigationalSystemWarningException("Cannot go beyond the map");
//            }
//            if (currentX > mapMaxX) {
//                currentX -= 1;
//                throw new NavigationalSystemWarningException("Cannot go beyond the map");
//            }
//            if (currentY > mapMaxY) {
//                currentY -= 1;
//                throw new NavigationalSystemWarningException("Cannot go beyond the map");
//            }
        }
        if (instruction == Instruction.L) {
            switch (currentOrientation) {
                case N:
                    currentOrientation = Orientation.W;
                case S:
                    currentOrientation = Orientation.E;
                case W:
                    currentOrientation = Orientation.S;
                case E:
                    currentOrientation = Orientation.N;
                default:
                    // do nothing
            }
        }
        if (instruction == Instruction.R) {
            switch (currentOrientation) {
                case N:
                    currentOrientation = Orientation.E;
                case S:
                    currentOrientation = Orientation.W;
                case W:
                    currentOrientation = Orientation.N;
                case E:
                    currentOrientation = Orientation.S;
                default:
                    // do nothing
            }
        }
    }

    public void landing(Vector vector) {
        currentOrientation = vector.getOrientation();
        currentX = vector.getX();
        currentY = vector.getY();
    }

    public void report() {
        System.out.print(name);
        System.out.println(" (" + currentX + ", " + currentY + ") " + currentOrientation);
    }
}
